<?php
/*
 * Template Name: Online Users
 */

get_header();
?>

<section>

    <div class="breadcum">

        <div class="container">

            <div class="page_title">

                <?php the_title('<h1>', '</h1>'); ?>

            </div>

        </div>

    </div>

</section>

<section>
    <div class="container">
        <?php
        if (have_posts()) :
            while (have_posts()) : the_post();
                the_content();
            endwhile;
        endif;
        ?>
    </div>
</section>

<?php get_footer(); ?>