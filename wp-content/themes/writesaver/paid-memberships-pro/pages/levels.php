<?php
global $wpdb, $pmpro_msg, $pmpro_msgt, $current_user, $pmpro_currency, $pmpro_currency_symbol, $pmpro_currencies;

$pmpro_levels = pmpro_getAllLevels(false, true);
//echo '<pre>';
//print_r($pmpro_levels);exit;
$pmpro_level_order = pmpro_getOption('level_order');

if (!empty($pmpro_level_order)) {
    $order = explode(',', $pmpro_level_order);

    //reorder array
    $reordered_levels = array();
    foreach ($order as $level_id) {
        foreach ($pmpro_levels as $key => $level) {
            if ($level_id == $level->id)
                $reordered_levels[] = $pmpro_levels[$key];
        }
    }

    $pmpro_levels = $reordered_levels;
}

$pmpro_levels = apply_filters("pmpro_levels_array", $pmpro_levels);
$currency_code = get_option('pmpro_currency');

$user_id = get_current_user_id();
$paypal_result = $wpdb->get_results("SELECT * FROM tbl_customer_general_info WHERE fk_customer_id = " . $user_id . " LIMIT 1 ");
$wp_one_time_purchase = $wpdb->get_row("SELECT * FROM wp_one_time_purchase WHERE id = 1");
if ($pmpro_msg) {
    ?>
    <div class="pmpro_message <?php echo $pmpro_msgt ?>"><?php echo $pmpro_msg ?></div>
    <?php
}
?>
<?php
if (isset($_REQUEST['add_paypal']) || isset($_REQUEST['add_paypal1']) || isset($_REQUEST['paypal_id'])) {
    $paypal_id = $_REQUEST['paypal_id'];
    $paypal_pay_amount = round($_REQUEST['paypal_pay_amount'], 2);
    $paypal_extra_words = $_REQUEST['paypal_extra_words'];

    $date = date('Y-m-d h:i:s');
    $descr = $wp_one_time_purchase->name . " charges for " . $paypal_extra_words . " words";
    $wpdb->insert('wp_price_per_extra_words', array('fk_customer_id' => $user_id, 'payment_date' => $date, 'price' => $paypal_pay_amount, 'descriptions' => $descr, 'payment_source' => 'PayPal', 'status' => 0, 'words' => $paypal_extra_words));
    $ins_id = $wpdb->insert_id;

    $wpdb->update("tbl_customer_general_info", array('paypal_id' => $paypal_id), array('fk_customer_id' => $user_id));

    $business_email = get_option('pmpro_gateway_email');
    $gateway_environment = get_option('pmpro_gateway_environment');
    if ($gateway_environment == 'sandbox') {
        $URL = "https://www.sandbox.paypal.com/cgi-bin/webscr";
    } elseif ($gateway_environment == 'live') {
        $URL = "https://www.paypal.com/cgi-bin/webscr";
    }

    $currency_code = get_option('pmpro_currency');

    $notify = get_theme_root_uri() . '/' . get_template() . '/notify-paypal.php'; //get_site_url()."/notify-paypal.php";
    $success = get_the_permalink(762) . "/?token=xs00u8c9cxd&level=success&ins_id=$ins_id";
    $cancel = get_the_permalink(762) . "/?token=xd0eu8c9cxd&level=cancel";
    $i = 1;
    ?>

    <div class="wrapper">
        <div style="text-align:center; margin-top:30px; font-size:24px;">Please wait...Loading PayPal...<br/><br/>Don't Refresh the page.</div>
        <div style="text-align:center; margin-top:20px;">
        </div>

        <form action="<?php echo $URL; ?>" method="post" name="ckout" id="ckout">
            <input type="text" name="cmd" value="_cart">
            <input type="text" name="upload" value="1">
            <input type="text" name="business" value="<?php echo $business_email; ?>">
            <input type="text" name="item_number_<?php echo $i; ?>" value="<?php echo $ins_id; ?>">
            <input type="text" name="item_name_<?php echo $i; ?>" value="<?php echo $descr; ?>">
            <input type="text" name="quantity_<?php echo $i; ?>" value="1">
            <input type="text" name="amount_<?php echo $i; ?>" value="<?php echo $paypal_pay_amount; ?>">
            <input type="text" name="shipping_1" value="0">
            <input type="text" name="discount_amount_cart" value="0">
            <input type="text" name="currency_code" value="<?php echo $currency_code; ?>">
            <input type="text" name="custom" value="<?php echo $ins_id; ?>">
            <input type="text" name="notify_url" value="<?php echo $notify; ?>">
            <input type="text" name="return" value="<?php echo $success; ?>">
            <input type="text" name="cancel_return" value="<?php echo $cancel; ?>">
        </form>
        <script type="text/javascript">
            document.ckout.submit();
        </script>
        <?php
    }

    include 'stripe/Stripe.php';

    Stripe::setApiKey(pmpro_getOption("stripe_secretkey"));

    if (isset($_POST['stripeToken'])) {

        $user_id = get_current_user_id();
        $cardno = encrypt_string($_REQUEST['cardno']);
        $expdate = $_REQUEST['expdate'];
        $expyear = $_REQUEST['expyear'];
        $securitycode = $_REQUEST['securitycode'];

        $prefix = $wpdb->prefix;
        $table_name = $prefix . 'creditdebit_card_details';
        $user = $wpdb->get_results("SELECT * FROM $table_name WHERE customer_id = $user_id");
        $user_count = count($user);
        if ($user_count > 0) {
            $insert = $wpdb->update(
                    $table_name, array(
                'cardnumber' => $cardno,
                'expirymonth' => $expdate,
                'expyear' => $expyear,
                'securitycode' => $securitycode,
                    ), array('customer_id' => $user_id)
            );
        } else {
            $insert = $wpdb->insert(
                    $table_name, array(
                'customer_id' => $user_id,
                'cardnumber' => $cardno,
                'expirymonth' => $expdate,
                'expyear' => $expyear,
                'securitycode' => $securitycode,
                'createddate' => date('Y-m-d')
                    )
            );
        }

        $amount_cents = str_replace(".", "", $_POST['card_price']);
        $price = $_POST['card_price'];
        $txt_extra_words = $_POST['txt_extra_words'];
        $dashboard_url = get_the_permalink(762);
        try {
            if($_POST['level'] == "custom") {
                /* In case of One-time payment */
            $date = date('Y-m-d h:i:s');
            $description = $wp_one_time_purchase->name . " charges for " . $txt_extra_words . " words";
            $wpdb->insert('wp_price_per_extra_words', array('fk_customer_id' => $user_id, 'stripe_reference' => '', 'payment_date' => $date, 'price' => $price, 'descriptions' => $description, 'payment_source' => 'Stripe', 'words' => $txt_extra_words, 'status' => 0));
            $ins_id = $wpdb->insert_id;
            $charge = Stripe_Charge::create(array(
                        "amount" => $amount_cents,
                        "currency" => "usd",
                        "source" => $_POST['stripeToken'],
                        "description" => $description)
            );

            $chargeArray = $charge->__toArray(true);
            $stripe_reference = $chargeArray['id'];

            $wpdb->update("wp_price_per_extra_words", array('stripe_reference' => $stripe_reference), array('id' => $ins_id));
            echo "<script type='text/javascript'>window.location.href='$dashboard_url/?token=xs00u8c9cxd&level=success&ins_id=$ins_id';</script>";
            } else {
                /* In case of Membership payment */
                $pmpro_level = pmpro_getLevel($_POST['level']);
                $description = "Subscription for ".$pmpro_level->name;

                $charge = Stripe_Charge::create(array(
                            "amount" => $amount_cents,
                            "currency" => "usd",
                            "source" => $_POST['stripeToken'],
                            "description" => $description)
                );
                $chargeArray = $charge->__toArray(true);
                $stripe_reference = $chargeArray['id'];
                $order = new MemberOrder();
                $str = "INSERT INTO wp_pmpro_membership_orders set `code`='" . $order->getRandomCode() . "', `session_id`='" . session_id() . "', `user_id`='" . $user_id . "', `membership_id`='" . $_POST['level'] . "', `paypal_token`='', `billing_name`='', `billing_street`='', `billing_city`='', `billing_state`='', `billing_zip`='', `billing_country`='', `billing_phone`='', `subtotal`='" . $price . "', `tax`=0, `couponamount`='', `certificate_id`='', `certificateamount`='', `total`='" . $price . "', `payment_type`='Stripe', `cardtype`='', `accountnumber`='', `expirationmonth`='', `expirationyear`='', `status`='success', `gateway`='stripe', `gateway_environment`='', `payment_transaction_id`='" . $stripe_reference . "', `subscription_transaction_id`='" . $stripe_reference . "', `timestamp`=now(), `affiliate_id`='', `affiliate_subid`='', `notes`='', `checkout_id`=''";
                $wpdb->query($str);
                $plan_id = $_POST['level'];
                $startdate = current_time("mysql");
                //calculate the end date
                if (!empty($pmpro_level->expiration_number)) {
                    $enddate = date_i18n("Y-m-d", strtotime("+ " . $pmpro_level->expiration_number . " " . $pmpro_level->expiration_period, current_time("timestamp")));
                } else {
                    $enddate = "NULL";
                }
                $custom_level = array(
                    'user_id' => $user_id,
                    'membership_id' => $pmpro_level->id,
                    'code_id' => "",
                    'initial_payment' => $pmpro_level->initial_payment,
                    'billing_amount' => $pmpro_level->billing_amount,
                    'cycle_number' => $pmpro_level->cycle_number,
                    'cycle_period' => $pmpro_level->cycle_period,
                    'billing_limit' => $pmpro_level->billing_limit,
                    'trial_amount' => $pmpro_level->trial_amount,
                    'trial_limit' => $pmpro_level->trial_limit,
                    'startdate' => $startdate,
                    'enddate' => $enddate
                );
                pmpro_changeMembershipLevel($custom_level, $user_id, 'active');
                //update the current user
                global $current_user;
                if (!$current_user->ID && $user->ID) {
                    $current_user = $user;
                } //in case the user just signed up
                pmpro_set_current_user();
                /* Adding words and sending email per add_words in functions.php */
                $_GET['level']=$pmpro_level->id;
                add_words();
                echo "<script type='text/javascript'>window.location.href='$dashboard_url/?token=xs00u8c9cxd&level=success';</script>";
            }

        } catch (Stripe_CardError $e) {

            echo "<script type='text/javascript'>
			window.location.href='$dashboard_url/?token=xd0eu8c9cxd&level=declined';
			</script>";

            //$error = $e->getMessage();
        } catch (Stripe_InvalidRequestError $e) {

            //print_r($e->getMessage());exit;
            echo "<script type='text/javascript'>
			window.location.href='$dashboard_url/?token=xd0eu8c9cxd&level=declined';
			</script>";
        } catch (Stripe_AuthenticationError $e) {

            //print_r($e->getMessage());exit;
            echo "<script type='text/javascript'>
			window.location.href='$dashboard_url/?token=xd0eu8c9cxd&level=declined';
			</script>";
        } catch (Stripe_ApiConnectionError $e) {

            //print_r($e->getMessage());exit;
            echo "<script type='text/javascript'>
			window.location.href='$dashboard_url/?token=xd0eu8c9cxd&level=declined';
			</script>";
        } catch (Stripe_Error $e) {

            //print_r($e->getMessage());exit;
            echo "<script type='text/javascript'>
			window.location.href='$dashboard_url/?token=xd0eu8c9cxd&level=declined';
			</script>";
        } catch (Exception $e) {

        }
    }
    ?>


    <div class="all_plans pmpro_checkout"  id="pmpro_levels_table">
        <div class="row">
            <?php
            if ($wp_one_time_purchase):

                $order = new MemberOrder();
                $plan_id = $order->getLastMemberOrder($current_user->ID);
                if ($plan_id > 0) {
                    $user_is_subscribed = 'yes';

                    $plan_ids = $wpdb->get_row("SELECT * FROM wp_pmpro_membership_orders WHERE id = " . $plan_id);

                    $plan_id = $plan_ids->membership_id ? $plan_ids->membership_id : 0;
                    $per_words_price = $wpdb->get_row("SELECT price_per_additional_word FROM wp_pmpro_membership_levels WHERE id = " . $plan_id);

                    $price_per_words = $per_words_price->price_per_additional_word;
                    if ($price_per_words == "") {
                        $price_per_words = 0;
                    }
                } else {

                    $user_is_subscribed = 'no';
                    $price_per_words = $wp_one_time_purchase->price_per_word;
                }
                ?>
                <div class="col-sm-4 one_time ">
                    <div class="plan_box">
                        <div class="plan_price">
                            <span><?php echo $wp_one_time_purchase->name; ?></span>
                        </div>
                        <div class="plan_details">
                            <div class="plan_details_box">
                                <div class="plan_detail_txt_main">
                                    <div class="plan_detail_txt">

                                        <p><?php echo $wp_one_time_purchase->description; ?></p>

                                        <span><?php echo $pmpro_currency_symbol . $price_per_words; ?>/word</span>
                                        <div id="enter_word">
                                            <input type="hidden" id="hid_price_per_words" value="<?php echo $price_per_words; ?>" />
                                            <input type="text" placeholder="Number of words" id="hdntotalwords" name="hdntotalwords" class="only_num" maxlength="6" >
                                            <a href="javascript:;" id="save_words">Calculate</a> 
                                            <span id="dis_price">$0.00</span>
                                            <input type="hidden" id="total_words_price" name="total_words_price" />
                                        </div>
                                    </div>                                                
                                </div>
                                <div class="btn_blue">
                                    <button class="btn_sky" id="id_SaveProofRead" disabled="" data-level_id="custom">Purchase now</button>                                                        
                                </div>
                            </div>
                        </div>                       
                    </div> 
                </div>
                <?php
            endif;
            $count = 0;
            if ($pmpro_levels) {
                ?>
                <div class="col-sm-8 monthly_plan ">
                    <div class="long_term_plan">
                        <span>Monthly Subscription Plans</span>
                    </div>
                    <div class="plan_flexslider">
                        <ul class="slides">
                            <?php $trans=array();
                            foreach ($pmpro_levels as $level) {
                                if (isset($current_user->membership_level->ID))
                                    $current_level = ($current_user->membership_level->ID == $level->id);
                                else
                                    $current_level = false;
                                if ($level->most_popular == 1) {
                                    $class = "most_popular";
                                    $planclass = '';
                                } else {
                                    $class = "";
                                    $planclass = "odd";
                                }
                                ?>
                                <li class=" <?php echo $planclass; ?><?php if ($current_level == $level) { ?> active<?php } ?>">
                                    <div class="plan_box <?php echo $class; ?>">
                                        <div class="plan_price">
                                            <span><?php echo $level->name; ?></span>
                                            <span class="price">
                                                <?php
                                                if (pmpro_isLevelFree($level))
                                                    $cost_text = "<strong>" . __("Free", "pmpro") . "</strong>";
                                                else
                                                    $cost_text = pmpro_getLevelCost($level, true, true);
                                                $expiration_text = pmpro_getLevelExpiration($level);
                                                if (!empty($cost_text))
                                                    echo $cost_text;
                                                elseif (!empty($expiration_text))
                                                    echo $expiration_text;
                                                ?>
                                            </span>

                                        </div>
                                        <div class="plan_details">
                                            <div class="plan_details_box">
                                                <div class="plan_detail_txt_main">
                                                    <div class="plan_detail_txt">
                                                        <p class="words_per_month"><span><?php echo $level->plan_words; ?></span> words / <?php echo pmpro_translate_billing_period($level->cycle_period); ?></p>
                                                        <p class="words_per_month"><?php echo $pmpro_currency_symbol . $level->price_per_additional_word; ?> /additional word*</p>
                                                        <div class="plan_detail_text"><?php echo $level->description; ?></div>
                                                    </div>                                                
                                                </div>
                                                <div class="btn_blue">

                                                    <?php if (empty($current_user->membership_level->ID)) {			 
													
				$price=str_replace('&#36;', '',str_replace('/Month', '', $cost_text));						
				
				$trans []= array('id'=>$level->id, 'name'=>$level->name,    'category'=>'Monthly Subscription Plans', 'list'=>'Plan', 'position'=>$level->id ,'price'=>$price); 
	
				?>
                                                        <a class=" pmpro_btn-select btn_sky class_bill_method" 
														data-name="<?=$level->name?>" 
														data-price="<?=$price?>" 
														data-sku="<?= $level->id?>" 
														data-id_sku="<?= $level->id?>" 
														data-list="Plan" 
														data-category="Monthly Subscription Plans" 
														id="<?php echo $level->id; ?>" data-price="<?=$level->initial_payment?>" href="javascript:void(0);"><?php _e('Purchase now', 'pmpro'); ?></a>
                                                    <?php } elseif (!$current_level) { ?>                	
                                                        <a class=" pmpro_btn-select btn_sky class_bill_method" data-price="<?=$level->initial_payment?>" data-words="<?=$level->plan_words?>" id="<?php echo $level->id; ?>" href="javascript:void(0);"><?php _e('Purchase now', 'pmpro'); ?></a>
                                                    <?php } elseif ($current_level) { ?>      

                                                        <?php
                                                        //if it's a one-time-payment level, offer a link to renew				
                                                        if (pmpro_isLevelExpiringSoon($current_user->membership_level) && $current_user->membership_level->allow_signups) {
                                                            ?>
                                                            <a class=" pmpro_btn-select btn_sky class_bill_method" id="<?php echo $level->id; ?>" data-price="<?=$level->initial_payment?>" data-words="<?=$level->plan_words?>" href="javascript:void(0);"><?php _e('Upgrade Plan', 'pmpro'); ?></a>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <a class=" disabled btn_sky class_bill_method" id="<?php echo $level->id; ?>" data-price="<?=$level->initial_payment?>" data-words="<?=$level->plan_words?>" href="javascript:void(0);"><?php _e('Purchase now', 'pmpro'); ?></a>
                                                            <?php
                                                        }
                                                        ?>

                                                    <?php } ?>                               
                                                </div>
                                            </div>
                                        </div>
                                        <?php if ($level->most_popular == 1): ?>
                                            <div class="most_popular_ad">
                                                <span> Most popular</span>
                                            </div>
                                        <?php endif; ?>
                                    </div> 
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?php
            }			
// Function to return the JavaScript representation of a TransactionData object.
function getItemJs(&$trans) {
  return <<<HTML
ga('ec:addImpression', {
  'id': '{$trans['id']}',
  'name': '{$trans['name']}',
  'price':'{$trans['price']}'
});
HTML;
}

/*
  'category': '{$trans['category']}',
  'list': '{$trans['list']}',
  'position': '{$trans['position']}',
*/

// Function to return the JavaScript representation of an ItemData object.
            ?>
			<script>
			//Google Ecommerce

			ga('require', 'ec');
			<?php
			foreach ($trans as $item) {
			  echo getItemJs($item);
			}
			?>
			ga('send', 'pageview'); 
			</script>
			
            <script>
                // new code
                jQuery(document).ready(function ($) {

                    $('.class_bill_method').click(function () {
                      //  debugger;
                        var level_id = $(this).attr('id');
                        $('#hid_mamu_level_id').val(level_id);
                        var price = parseInt($(this).attr('data-price'));
                        $('#hid_mamu_price').val(price.toFixed(2));
                        $('#hid_mamu_words').val($(this).attr('data-words'));
                        var url = '<?php echo home_url(); ?>/paypal-process/?level=' + level_id;
                        $('#lbl_paypal_submit_link').attr("href", url);
                        $('#lbl_paypal_submit_link').removeAttr("data-toggle");
                        $('#lbl_paypal_submit_link').removeAttr("data-target");
                        $('#add_paypal').prop('type', 'button');
                        $('#add_paypal').val('Update');
                        if ($('#current_user_ID').val() == '') {
                            $('#hid_level_id').val(level_id);
                            open_loginpop();
                            return false;
                        } else {
                             $('#payment_option_modal .paypal_billing').hide();
                            $('#payment_option_modal').fadeIn();
                            $('#payment_option_modal').removeClass('open');
                            return false;
                        }
                    });
                    $('#openBtn1').click(function () {
                        //debugger;
                        var pay_type = $(this).attr('data-card-type');
                        var level_id = $('#hid_mamu_level_id').val();
                        if (level_id == 'custom') {
                            $('#Paypal').fadeIn();
                            $('#Paypal').addClass('open');
                            return false;
                        }
                        var mamu_url = $('#hid_mamu_url').val();
                        var iframe_url = mamu_url + "level=" + level_id + "&i=i&pay_type=" + pay_type;
                        $('#frame_plan1').attr('src', iframe_url);
                        setTimeout(function () {
                            $('#myModal1').fadeOut();
                            $('#myModal1').addClass('open');
                            $('#myModal1').css('display', 'block');
                        }, 3000);

                        return false;
                    });

                    $('#openBtn2').click(function () {

                        var pay_type = $(this).attr('data-card-type');
                        var level_id = $('#hid_mamu_level_id').val();
                        var mamu_url = $('#hid_mamu_url').val();

                        $('#frame_plan1').attr('src', mamu_url + "level=" + level_id + "&i=i&pay_type=" + pay_type)

                        $('#myModal1').fadeOut();
                        $('#myModal1').addClass('open');
                        $('#myModal1').css('display', 'block');

                        return false;
                    });

                    $('#payment_option_modal a.ppmodal .fa-remove').click(function (e) {
                        e.stopPropagation();
                        $('#payment_option_modal').fadeOut();
                        $('#payment_option_modal').removeClass('open');
                    });

                    $('#myModal1 a.ppmodal .fa-remove').click(function (e) {
                        $('#myModal1').fadeIn();
                        $('#myModal1').removeClass('open');
                        $('#myModal1').css('display', 'none');
                    });
                });
                function goToPaypal() {
                   // debugger;
                    var paypal_id = $('#paypal_id').val().trim();
                    if (paypal_id == "") {
                        $('#paypal_id').focus();
                        return false;
                    }
                    $.post("<?php echo get_template_directory_uri(); ?>/ajax.php", {"choice": "update_paypal", "paypal_id": paypal_id}, function (result) {

                        var tmp = result.split('~');
                        if (tmp[0] == "yes") {

                            var level_id = $('#hid_mamu_level_id').val();
                            if (level_id == 'custom') {
                                return false;
                            }
                            var url = '<?php echo home_url(); ?>/paypal-process/?level=' + level_id;
                            window.location = url;
                        }
                    });
                }
            </script>



            <!--Billing Method-->
            <input type="hidden" id="hid_mamu_level_id" value="<?php ($_REQUEST['plan']) ? $_REQUEST['plan'] : ''; ?>" />
            <input type="hidden" id="hid_mamu_price" value="" />
            <input type="hidden" id="hid_mamu_words" value="" />
            <input type="hidden" id="hid_mamu_url" value="<?php echo get_site_url(); ?>/membership-account/membership-checkout/?" />
            <input type="hidden" id="hid_mamu_pay_type" value="" />
            <div id="payment_option_modal" class="pop_overlay" style="display: none;">
                <div class="pop_main" style="padding: 0px; height: 250px; margin: auto;">
                    <div class="pop_head" style="min-height:0;">
                        <a href="javascript:void(0);" data-dismiss="modal" type="button" class="ppmodal" ><i class="fa fa-remove"  aria-hidden="true"></i></a>
                    </div>
                    <div class="pop_body">
                        <div class="setting_right">
                            <div class="field_title">
                                <h4>Billing Methods</h4>
                            </div>
                            <div class="">
                                <div class="billing_table_responsive">
                                    <div class="billing_table">
                                        <div class="customer_billing_methods paypal_billing">
                                            <div class="billing_image">
                                                <img src="<?php echo get_template_directory_uri() ?>/images/paypal.png" alt="paypal">
                                            </div>
                                            <div class="billing_type">
                                                <p>Paypal</p>
                                            </div>
                                            <div class="bill_popup" style="width:50%;">
                                                <?php if ($paypal_result[0]->paypal_id == "") { ?>
                                                    <a href="#" role="button" data-toggle="modal" data-target="#paypal_modal" onclick="myga('Paypal')" data-card-type="paypal" class="paypal_pop btn_sky">Set up</a>
                                                <?php } else { ?>
                                                    <a href="#" role="button" id="lbl_paypal_submit_link" onclick="myga('Proceed')" class="paypal_pop btn_sky">Proceed</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="customer_billing_methods">
                                            <div class="billing_image">
                                                <img src="<?php echo get_template_directory_uri() ?>/images/credit_debit.png" alt="paypal">
                                            </div>
                                            <div class="billing_type">
                                                <p>Credit or debit card</p>
                                            </div>
                                            <div class="bill_popup">
                                                <a href="javascript:void(0);" class="btn_sky pop_btn" data-toggle="modal" onclick="myga('Credit card')" data-card-type="card" id="openBtn1">Set up</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--Paypal Method-->
            <div id="paypal_modal" class="pop_overlay" style="display: none;">
                <div class="pop_main" style="bottom:30%;">
                    <div class="pop_head">
                        <a href="javascript:void(0);" data-dismiss="modal" type="button" ><i class="fa fa-remove"  aria-hidden="true"></i></a>
                    </div>
                    <div class="pop_body">
                        <div class="page_title">
                            <h2>Add a Paypal</h2>
                            <h4>Payment information</h4>
                        </div>
                        <div class="pop_content">

                            <form method="post" action="" id="cust_paypal" autocomplete="off">
                                <div class="row">
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <input type="email" class="contact_block" name="paypal_id" id="paypal_id" value="<?php echo $paypal_result[0]->paypal_id; ?>" maxlength="100"  placeholder="Paypal Id*">
                                    </div>
                                    <div class="buttons col-sm-12" style="margin-top:2%;">
                                        <input type="button" name="add_paypal" id="add_paypal" value="Update" class="btn_sky" style="float:none;" onclick="goToPaypal();">
                                        <input data-dismiss="modal" type="button" id="close_paypal_model" value="Cancel" class="btn_sky">
                                        <input type="hidden" name="paypal_extra_words" id="paypal_extra_words" />
                                        <input type="hidden" name="paypal_pay_amount" id="paypal_pay_amount" />
                                    </div>
                                    <div class="paypal_msg"></div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <div id="Paypal" class="pop_overlay open" style="display: none;">
                <div class="pop_main">
                    <div class="pop_head">
                        <a href="javascript:void(0);"><i class="fa fa-remove" aria-hidden="true"></i></a>
                    </div>
                    <div class="pop_body">
                        <div class="page_title">
                            <h2>Add a credit or debit card</h2>
                            <h4>Payment information</h4>
                        </div>
                        <div class="pop_content">            	
                            <form action="" method="POST" id="payment-form" autocomplete="off"  >
                                <?php
                                $user_id = get_current_user_id();

                                $prefix = $wpdb->prefix;
                                $table_name = $prefix . 'creditdebit_card_details';
                                $user_infio = $wpdb->get_row("SELECT * FROM $table_name WHERE customer_id = $user_id");
                                ?>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <span class="payment-errors" style="color:#F00;"></span>
                                    </div>
                                    <div class="col-sm-12">
                                        <input type="text" value="<?php echo decrypt_string($user_infio->cardnumber); ?>" name="cardno" id="cardno" placeholder="Card number" value="" data-stripe="number" class="only_num contact_block" maxlength="16">
                                        <div><span id="errorcardno" style="color:red;"></span></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="contact_block" id="expdate" name="expdate" data-stripe="exp_month">
                                            <option>Expiration Month</option>
                                            <option value="01" <?php echo ($user_infio->expirymonth == '01') ? "selected" : ''; ?>>01</option>
                                            <option value="02" <?php echo ($user_infio->expirymonth == '02' ) ? "selected" : ''; ?>>02</option>
                                            <option value="03" <?php echo ($user_infio->expirymonth == '03') ? "selected" : ''; ?>>03</option>
                                            <option value="04" <?php echo ($user_infio->expirymonth == '04' ) ? "selected" : ''; ?>>04</option>
                                            <option value="05" <?php echo ($user_infio->expirymonth == '05') ? "selected" : ''; ?>>05</option>
                                            <option value="06" <?php echo ($user_infio->expirymonth == '06' ) ? "selected" : ''; ?>>06</option>
                                            <option value="07" <?php echo ($user_infio->expirymonth == '07') ? "selected" : ''; ?>>07</option>
                                            <option value="08" <?php echo ($user_infio->expirymonth == '08' ) ? "selected" : ''; ?>>08</option>
                                            <option value="09" <?php echo ($user_infio->expirymonth == '09') ? "selected" : ''; ?>>09</option>
                                            <option value="10" <?php echo ($user_infio->expirymonth == '10' ) ? "selected" : ''; ?>>10</option>
                                            <option value="11" <?php echo ($user_infio->expirymonth == '11') ? "selected" : ''; ?>>11</option>
                                            <option value="12" <?php echo ($user_infio->expirymonth == '12' ) ? "selected" : ''; ?>>12</option>
                                        </select>
                                        <div><span id="errorexpdate" style="color:red;"></span></div> 
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="contact_block" id="expyear" name="expyear" data-stripe="exp_year">
                                            <option>Year</option>
                                            <?php
                                            $next_yr = date('Y') + 12;
                                            for ($i = date('Y'); $i < $next_yr; $i++) {
                                                ?>
                                                <option value="<?php echo $i; ?>" <?php echo ($user_infio->expyear == $i) ? "selected" : ''; ?>><?php echo $i; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div><span id="errorexpyear" style="color:red;"></span></div> 
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" value="<?php echo $user_infio->securitycode; ?>" maxlength="3" placeholder="Security Code*" id="securitycode" name="securitycode" class="only_num contact_block" data-stripe="cvc" />
                                        <div><span id="errorsecuritycode" style="color:red;"></span></div> 
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="contact_block">What's this?</label>
                                    </div>
                                </div>
                                <div class="buttons">
                                    <input type="submit" value="Make Payment" class="btn_sky" id="btnAddCart"  />
                                    <input type="reset" value="Cancel" id="close_paypal"  data-dismiss="modal" class="btn_sky" />
                                </div>
                                <div class="card_msg"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Plan Details Modal -->
            <div id="myModal1" class="pop_overlay open" style="display: none;">
                <div class="pop_main" style="height:550px;bottom:30%; width:430px;">
                    <div class="pop_head" style="min-height:0;">
                        <a href="javascript:void(0);" data-dismiss="modal" type="button" class="ppmodal" ><i class="fa fa-remove"  aria-hidden="true"></i></a>
                    </div>
                    <div class="pop_body" style="margin-left:-30px">
                        <iframe id="frame_plan1" src="<?php echo get_site_url(); ?>/membership-account/membership-checkout/?level=1&pay_type=card" style="width:430px; height:100%; border:none;"></iframe>
                    </div>
                </div>
            </div>
            <!-- /Plan Details #myModal -->
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
                                            Stripe.setPublishableKey('<?php echo pmpro_getOption("stripe_publishablekey"); ?>');

                                            $(function () {
                                                var $form = $('#payment-form');

                                                $form.submit(function (event) {
                                                    if ($("#cardno").val() == "") {
                                                        $("#errorcardno").html("Please enter card number");
                                                        $("#cardno").focus();
                                                        return false;
                                                    } else {
                                                        $("#errorcardno").html("");
                                                        if ($("#cardno").val().length < 16) {
                                                            $("#errorcardno").html("Please enter valid card number");
                                                            $("#cardno").focus();
                                                            return false;
                                                        }
                                                    }
                                                    if ($("#expdate").val() == "Expiration Month") {
                                                        $("#errorexpdate").html("Please select expiry month");
                                                        $("#expdate").focus();
                                                        return false;
                                                    } else {
                                                        $("#errorexpdate").html("");
                                                    }
                                                    if ($("#expyear").val() == "Year") {
                                                        $("#errorexpyear").html("Please select expiry year");
                                                        $("#expyear").focus();
                                                        return false;
                                                    } else {
                                                        $("#errorexpyear").html("");
                                                    }
                                                    if ($("#securitycode").val() == "") {
                                                        $("#errorsecuritycode").html("Please enter cvv no");
                                                        $("#securitycode").focus();
                                                        return false;
                                                    } else {
                                                        $("#errorsecuritycode").html("");
                                                    }

                                                    // Disable the submit button to prevent repeated clicks:
                                                    $form.find('.submit').prop('disabled', true);

                                                    // Request a token from Stripe:
                                                    Stripe.card.createToken($form, stripeResponseHandler);

                                                    // Prevent the form from being submitted:
                                                    return false;
                                                });
                                            });

                                            function stripeResponseHandler(status, response) {
                                                // Grab the form:
                                                var $form = $('#payment-form');

                                                if (response.error) { // Problem!

                                                    // Show the errors on the form:
                                                    $form.find('.payment-errors').text(response.error.message);
                                                    $form.find('.submit').prop('disabled', false); // Re-enable submission

                                                } else { // Token was created!

                                                    // Get the token ID:
                                                    var token = response.id;

                                                    // Price
                                                    if($('#hid_mamu_level_id').val()== "custom") {
                                                        var price = $('#total_words_price').val();
                                                        var totalwords = $("#hdntotalwords").val().trim();
                                                        var txt_extra_words = parseInt(totalwords);
                                                    } else {
                                                        var price = $('#hid_mamu_price').val();
                                                        var txt_extra_words = $('#hid_mamu_words').val();
                                                    }
                                                    $form.append($('<input type="hidden" name="card_price">').val(price));
                                                    $form.append($('<input type="hidden" name="txt_extra_words">').val(txt_extra_words));
                                                    $form.append($('<input type="hidden" name="level">').val($('#hid_mamu_level_id').val()));

                                                    // Insert the token ID into the form so it gets submitted to the server:
                                                    $form.append($('<input type="hidden" name="stripeToken">').val(token));

                                                    // Submit the form:
                                                    $form.get(0).submit();
                                                }
                                            }
                                            ;
    </script>
    <script>
        $('#hdntotalwords').change(function () {
            $('#id_SaveProofRead').prop('disabled', true);
        });
        $('#save_words').click(function () {
            $('.pay_msg').remove();
            if ($("#hdntotalwords").val() == "") {
                $('#hdntotalwords').after('<span class="pay_msg text-danger">Enter No. of words</span>');
                return false;
            } else if ($("#hdntotalwords").val() <= 0) {
                $('#hdntotalwords').after('<span class="pay_msg text-danger">Enter word greater than 0.</span>');
                return false;
            } else if (isNaN($("#hdntotalwords").val())) {
                $('#hdntotalwords').after('<span class="pay_msg text-danger">Enter only numbers.</span>');
                return false;
            } else {
                var totalwords = $("#hdntotalwords").val().trim();
                total_words = parseInt(totalwords);
                var per_words = $('#hid_price_per_words').val();
                var lbl_priceperwords = parseInt(total_words) * parseFloat(per_words);
                $('#dis_price').html('$' + lbl_priceperwords.toFixed(2));
                $('#save_words').text('Recalculate');
                $('#id_SaveProofRead').prop('disabled', false);
            }
        });

        $("#id_SaveProofRead").click(function ()
        {
            $('.pay_msg').remove();
            if ($('#current_user_ID').val() == '') {
                $('#hid_mamu_level_id').val('custom');
                open_loginpop();
                return false;
            } else {

                var totalwords = $("#hdntotalwords").val().trim();
                total_words = parseInt(totalwords);

                var per_words = $('#hid_price_per_words').val();

                //alert(per_words)
                var lbl_priceperwords = parseInt(total_words) * parseFloat(per_words);
                $('#total_words_price').val(lbl_priceperwords.toFixed(2));

                $('#paypal_extra_words').val(total_words);
                $('#paypal_pay_amount').val(lbl_priceperwords);

                var level_id = $(this).attr('data-level_id');
                $('#hid_mamu_level_id').val(level_id);
                $('#lbl_paypal_submit_link').attr("href", '#');
                $('#lbl_paypal_submit_link').attr("data-toggle", 'modal');
                $('#lbl_paypal_submit_link').attr("data-target", '#paypal_modal');
                $('#lbl_paypal_submit_link').text('Set up');
                $('#add_paypal').prop('type', 'submit');
                $('#add_paypal').val('Make Payment');

                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                    'event': 'Virtual Doc Submit Payment View',
                    'virtualPageURL': '/extra/pay',
                    'virtualPageTitle': 'Modal pay for document extra words',
                    'price_of_doc': lbl_priceperwords.toFixed(2)
                });
                $('#payment_option_modal .paypal_billing').show();
                $('#payment_option_modal').fadeIn();
                $('#payment_option_modal').addClass('open');
                $('#payment_option_modal').css('display', 'block');

                return false;
            }

        });
    </script>