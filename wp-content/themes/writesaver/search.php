<?php
/*
 * Template Name: search
 */

get_header();
?>
<section>
    <div class="breadcum">
        <div class="container">
            <div class="page_title">
                <?php //the_title('<h1>', '</h1>'); ?>
            </div>
        </div>
    </div>
</section>

<section>
    <div id="main_blog">
        <div class="blog_category_sticky">
            <div class="container">
                <div class="blog_category_sticky_right">
                    <div class="search_box">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="blog_category_main" id="notContent">
            <div class="container">
                <div id="<?php echo $term->slug; ?>" class="blog_listt"> 
                    <?php if (have_posts()): ?>
                        <h2>Search Results for '<?php echo get_search_query() ?>'</h2> 
                        <?php while (have_posts()) : the_post(); ?>

                            <div class="blog_block"> 
                                <div class="blog_img">
                                    <img src="<?php the_post_thumbnail_url(array(1140, 478)); ?>" class="img-responsive" alt="">
                                    <div class="blog_textblock">
                                        <div class="blog_txt">
                                            <div class="blog_title">
                                                <h4>
                                                    <?php
                                                    $len = strlen(get_the_title());
                                                    if ($len > 80):
                                                        echo substr(get_the_title(), 0, 80) . "...";
                                                    else:
                                                        echo get_the_title();
                                                    endif;
                                                    ?>
                                                </h4>
                                            </div>
                                            <div class="blog_date">
                                                <span>   <?php the_date(); ?></span>
                                            </div>
                                            <div class="blog_desc">
                                                <?php the_excerpt(); ?>   
                                            </div>
                                            <div class="blog_social">
                                                <ul>
                                                    <li><?php echo do_shortcode('[TheChamp-Sharing]') ?></li>
                                                </ul>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="hover_effect_blog">
                                    <a href="<?php the_permalink() ?>">
                                        <div class="hover_effect_blog_img">                                        
                                            <img src="<?php echo get_template_directory_uri() ?>/images/sky_arrow.png" class="img-responsive">
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; ?>

                        <?php wp_reset_postdata(); ?>                                           
                        <?php
                        if (function_exists(custom_pagination)) {
                            custom_pagination($the_query->max_num_pages, "", $paged);
                        }
                        ?>
                    <?php else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>

                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>