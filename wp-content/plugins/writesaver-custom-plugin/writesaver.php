<?php

/*
  Plugin Name: Writesaver-Custom-Plugin
  Plugin URI: www.aliansoftware.net
  Description: This plugin is developed to support writesaver with wordpress
  Version: 1.0.0
  Author: Aliansoftware
  Author URI: www.aliansoftware.net
  License: GPLv2
 */
define('WRITESAVER_CUSTOM_PLUGIN_URL', plugins_url('', __FILE__));

function alian_writesaver_include() {
    require_once plugin_dir_path(__FILE__) . '/include/writesaver-functions.php';
    require_once plugin_dir_path(__FILE__) . '/include/writesaver-ajax-admin.php';
}

add_action('init', 'alian_writesaver_include');
